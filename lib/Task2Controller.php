<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/1/14 12:11 AM
 * @version 1.0
 */


namespace TestLib;

class Task2Controller extends TaskAbstract implements iController
{
    const EARTH_RADIUS_KM = 6371; // https://en.wikipedia.org/wiki/Earth_radius

    public function response()
    {
        $timeStart = date('Y-m-d', time() - 1 * 3600 * 24);
        $timeEnd = date('Y-m-d', time());


        $jsonFile = $this->getCacheDir() . DIRECTORY_SEPARATOR . $_GET['apiKey'] . '.json';
        if (!file_exists($jsonFile)) {
            throw new SeeMeException('No object ids. Please load data in task 1!', SeeMeException::CODE_PUBLIC);
        }
        $objectIds = json_decode(file_get_contents($jsonFile), true);
        if (empty($objectIds)) {
            throw new SeeMeException('No object ids. Please load data in task 1!', SeeMeException::CODE_PUBLIC);
        }

        $lastDataRaw = array();
        foreach ($objectIds as $objectId => $object1Name) {
            $url = sprintf('https://apps.oskando.ee/seeme/api/gethistory?key=%s&id=%d&startTimestamp=%s&endTimestamp=%s', $_GET['apiKey'], $objectId, $timeStart, $timeEnd);
            $lastDataRaw[$object1Name] = $this->getConvertedXMLData($url);

        }
        $legend = $this->makeLegend($lastDataRaw[$object1Name]);


        /**
         * data structue
         *
         *  A/B | nr | nr | nr
         * ----------------
         *  nr | x  | %
         *  nr | %  | x
         *  nr | %  | %
         */

        $polygons = array();
        $this->collectdata($lastDataRaw, $objectIds, $legend, $polygons);

        $tableData = array();
        foreach ($polygons as $object1Name => &$object1Route) {
            $tableData[$object1Name] = array();
            foreach ($polygons as $object2Name => &$object2Route) {
                if (!isset($tableData[$object1Name][$object2Name])) {
                    $tableData[$object1Name][$object2Name] = 0;
                }
                if ($object1Name == $object2Name) {
                    continue;
                }

                $largerRoute = count($object1Route) > count($object2Route) ? $object1Route : $object2Route;
                $smallerRoute = count($object1Route) < count($object2Route) ? $object1Route : $object2Route;
                $count = 0;
                $index2 = 0;
                foreach ($largerRoute as $index => &$coords) {
                    $rad1Lat = deg2rad($largerRoute[$index]['lat']);
                    $rad2Lat = deg2rad($smallerRoute[$index2]['lat']);
                    $rad1Lng = deg2rad($smallerRoute[$index2]['lng']);
                    $rad2Lng = deg2rad($largerRoute[$index]['lng']);
                    $dist = acos(
                                sin($rad1Lat) * sin($rad2Lat) +
                                cos($rad1Lat) * cos($rad2Lat) * cos($rad1Lng - $rad2Lng)
                            ) * self::EARTH_RADIUS_KM;


                    $tableData[$object1Name][$object2Name] += $dist;
                    $count++;
                    if ($index2 == count($smallerRoute) - 1) {
                        $index2 = 0;
                    } else {
                        $index2++;
                    }
                }
                if ($count > 0) {
                    $tableData[$object1Name][$object2Name] = round($tableData[$object1Name][$object2Name] / $count, 2);
                } else {
                    $tableData[$object1Name][$object2Name] = 0;
                }
            }
        }


        return array('table' => $tableData, 'polygons' => $polygons);
    }

    /**
     * @param $lastDataRaw
     * @param array $objectIds
     * @param $legend
     * @param $polygons
     * @internal param $objectId
     * @return array
     */
    public function collectdata($lastDataRaw, array $objectIds, $legend, &$polygons)
    {
        $dataAll = array();
        $data = array();
        foreach ($objectIds as $objectId) {
            if (isset($lastDataRaw[$objectId]['records']['@children']) && is_array($lastDataRaw[$objectId]['records']['@children'])) {
                $rPos = 0;
                foreach ($lastDataRaw[$objectId]['records']['@children'] as $record) {
                    if ($record['@name'] == 'record') {
                        $data[$rPos] = array();
                        $data[$rPos]['id'] = $objectId;
                        foreach ($record['@children'] as $rKey => $r) {
                            if (!isset($r['@text']) || strlen(trim($r['@text'])) == 0) {
                                continue;
                            }

                            if ($rKey == $legend['date']) {
                                $data[$rPos]['date'] = $r['@text'];
                            }
                            if ($rKey == $legend['arriveTimestamp']) {
                                $data[$rPos]['arriveTimestamp'] = $r['@text'];
                            }

                            if ($rKey == $legend['latitude']) {
                                $data[$rPos]['latitude'] = floatval($r['@text']);
                            }
                            if ($rKey == $legend['longitude']) {
                                $data[$rPos]['longitude'] = floatval($r['@text']);
                            }

                            if ($rKey == $legend['speed']) {
                                $data[$rPos]['speed'] = $r['@text'];
                            }
                            if ($rKey == $legend['enginestate']) {
                                $data[$rPos]['enginestate'] = $r['@text'];
                            }
                        }

                        $polygons[$objectId][$rPos] = array('lat' => $data[$rPos]['latitude'], 'lng' => $data[$rPos]['longitude']);

                        $rPos++;
                    } else {
                        continue;
                    }
                }
                $dataAll[$objectId] = $data;
            }
        }

        return $dataAll;
    }
} 