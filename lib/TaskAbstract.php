<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/9/14 7:27 PM
 * @version 1.0
 */

namespace TestLib;


class TaskAbstract
{
    /**
     * @param $url
     * @return array|mixed
     * @throws SeeMeException
     */
    public function getConvertedXMLData($url)
    {
        $dir = $this->getCacheDir();

        $streamContext = stream_context_create(
            array(
                'http' => array(
                    'timeout' => 15
                ),
                'https' => array(
                    'timeout' => 15
                )
            )
        );
        $data = file_get_contents($url, null, $streamContext);
        $cacheFile = $dir . DIRECTORY_SEPARATOR . md5($url);

        if ($data !== false) {
            $convertedData = XmlHelper::getSimpleXMLElementAsArray(new \SimpleXMLElement($data));
            file_put_contents($cacheFile . '.xml', $data);
            file_put_contents($cacheFile, serialize($convertedData));
        } else {
            $convertedData = unserialize(file_get_contents($cacheFile));
        }

        if ($convertedData === false) {
            throw new SeeMeException('No data');
        }

        return $convertedData;
    }


    /**
     * @param $lastDataRaw
     * @throws SeeMeException
     * @return array
     */
    public function makeLegend($lastDataRaw)
    {
        $legend = array();
        if (isset($lastDataRaw['legend']['@children']) && is_array($lastDataRaw['legend']['@children'])) {
            $lPos = 0;
            foreach ($lastDataRaw['legend']['@children'] as $legendElm) {
                $legend[$legendElm['@text']] = $lPos;
                $lPos++;
            }

            if (empty($legend)) {
                throw new SeeMeException('Cannot make legend!');
            }

            return $legend;
        }

        throw new SeeMeException('Cannot make legend!');
    }

    /**
     * @return string
     * @throws SeeMeException
     */
    public function getCacheDir()
    {
        $dir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'cache';

        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                throw new SeeMeException("Cannot make dir " . $dir);
            }
            return $dir;
        }
        return $dir;
    }
} 