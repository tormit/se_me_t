<?php
/**
 * @author: Tormi Talv <tormit@gmail.com> 2013
 * @since: 2/18/13 2:55 PM
 * @version 1.0
 */

namespace TestLib;

class XmlHelper
{

    /**
     * @param \SimpleXMLElement $simpleXmlObject
     * @param bool $onlyChildren
     * @param bool $firstIndexAsName
     * @return array
     */
    public static function getSimpleXMLElementAsArray(\SimpleXMLElement $simpleXmlObject, $onlyChildren = false, $firstIndexAsName = true)
    {
        $array = array();

        self::convertXmlObjToArr($simpleXmlObject, $array, $onlyChildren, $firstIndexAsName);

        return $array;
    }

    /**
     * @param \SimpleXMLElement $obj
     * @param $arr
     * @param bool $onlyChildren
     * @param bool $firstIndexAsName
     */
    private static function convertXmlObjToArr(\SimpleXMLElement $obj, &$arr, $onlyChildren = false, $firstIndexAsName = true)
    {
        if (!$onlyChildren) {
            $arr['@name'] = (string)$obj->getName();
            foreach ($obj->attributes() as $name => $value) {
                $arr['@attributes'][$name] = (string)$value;
            }
        }


        $children = $obj->children();
        $elementNames = array();
        foreach ($children as $elementName => $node) {
            $elementName = (string)$elementName;
            $nextIdx = count($arr);

            if ($firstIndexAsName === true && !isset($elementNames[$elementName])) {
                $elementNames[$elementName] = 1;
                $nextIdx = $elementName;
            }

            $arr[$nextIdx] = array();
            $arr[$nextIdx]['@name'] = $elementName;
            $arr[$nextIdx]['@attributes'] = array();

            $attributes = $node->attributes();
            foreach ($attributes as $attributeName => $attributeValue) {
                $attribName = trim((string)$attributeName);
                $attribVal = trim((string)$attributeValue);
                $arr[$nextIdx]['@attributes'][$attribName] = $attribVal;
            }

            $text = (string)$node;
            $text = trim($text);
            if (strlen($text) > 0) {
                $arr[$nextIdx]['@text'] = $text;
            }
            $arr[$nextIdx]['@children'] = array();
            self::convertXmlObjToArr($node, $arr[$nextIdx]['@children'], true);
        }
        return;
    }

}

