<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/1/14 12:55 AM
 * @version 1.0
 */

namespace TestLib;


interface iController
{
    public function response();
} 