<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/2/14 2:29 PM
 * @version 1.0
 */

namespace TestLib;


class SeeMeException extends \Exception
{
    const CODE_PUBLIC = 1;
} 