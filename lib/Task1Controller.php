<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/1/14 12:11 AM
 * @version 1.0
 */


namespace TestLib;

class Task1Controller extends TaskAbstract implements iController
{
    public function response()
    {
        $url = sprintf('https://apps.oskando.ee/seeme/api/getlastdata?key=%s', $_GET['apiKey']);
        $lastDataRaw = $this->getConvertedXMLData($url);

        $legend = $this->makeLegend($lastDataRaw);

        $data = array();

        $objectIds = array();
        if (isset($lastDataRaw['records']['@children']) && is_array($lastDataRaw['records']['@children'])) {
            $rPos = 0;
            foreach ($lastDataRaw['records']['@children'] as $record) {
                if ($record['@name'] == 'record') {
                    $data[$rPos] = array();
                    foreach ($record['@children'] as $rKey => $r) {
                        if (!isset($r['@text']) || strlen(trim($r['@text'])) == 0) {
                            continue;
                        }

                        if ($rKey == $legend['objectId']) {
                            $data[$rPos]['objectId'] = $r['@text'];
                        }
                        if ($rKey == $legend['objectName']) {
                            $data[$rPos]['vehicle'] = $r['@text'];
                        }
                        if ($rKey == $legend['speed']) {
                            $data[$rPos]['speed'] = $r['@text'];
                        }

                        if ($rKey == $legend['timestamp']) {
                            $data[$rPos]['timestamp'] = $r['@text'];
                        }
                        if ($rKey == $legend['ArriveTimestamp']) {
                            $data[$rPos]['ArriveTimestamp'] = $r['@text'];
                        }

                        if ($rKey == $legend['latitude']) {
                            $data[$rPos]['lat'] = $r['@text'];
                        }
                        if ($rKey == $legend['longitude']) {
                            $data[$rPos]['lng'] = $r['@text'];
                        }
                    }

                    $objectIds[$data[$rPos]['objectId']] = $data[$rPos]['vehicle'];

                    if (!isset($data[$rPos]['ArriveTimestamp'])) {
                        $data[$rPos]['ArriveTimestamp'] = date('Y-m-d H:i:sO');
                    }
                    $tz = new \DateTimeZone('Europe/Tallinn');
                    $dtUpdated = \DateTime::createFromFormat('Y-m-d H:i:sO', $data[$rPos]['timestamp'], $tz);

                    if ($dtUpdated instanceof \DateTime) {
                        $diff = time() - $dtUpdated->format('U');
                        $humanReadable = '';

                        $units = array(
                            2592000 => 'kuu',
                            86400 => 'päev',
                            3600 => 'tund',
                            60 => 'minut',
                            1 => 'sekund'
                        );
                        $unitsPlural = array(
                            2592000 => 'kuud',
                            86400 => 'päeva',
                            3600 => 'tundi',
                            60 => 'minutit',
                            1 => 'sekundit'
                        );

                        foreach ($units as $unit => $text) {
                            if ($diff < $unit) {
                                continue;
                            }

                            $numberOfUnits = floor($diff / $unit);
                            $unitText = $numberOfUnits > 0 ? $unitsPlural[$unit] : $text;
                            $humanReadable .= $numberOfUnits . ' ' . $unitText . ($unit > 1 ? ', ' : '');
                            $diff = $diff - $numberOfUnits * $unit;
                        }

                        $data[$rPos]['time'] = $humanReadable . ' tagasi';
                    } else {
                        $data[$rPos]['time'] = 'Teadmata';
                    }

                    $rPos++;
                } else {
                    continue;
                }
            }
        }

        // save objects for task 2
        file_put_contents($this->getCacheDir() . DIRECTORY_SEPARATOR . $_GET['apiKey'] . '.json', json_encode($objectIds));

        return $data;
    }
} 