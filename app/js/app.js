/**
 * Created by tormi on 3/1/14.
 */

var seemeApp = angular.module('seeMe', ['ngRoute', 'ui.map']).config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'home.html'
    });
    $routeProvider.when('/test1', {
        templateUrl: 'test1.html',
        controller: 'Test1Controller'
    });
    $routeProvider.when('/test2', {
        templateUrl: 'test2.html',
        controller: 'Test2Controller'
    });

    //$routeProvider.otherwise({redirectTo: '/'});
});

seemeApp.controller('Test1Controller', function ($scope, $http) {
        $scope.indicator = false;
        $scope.requestErrors = 0;
        $scope.table = [];
        $scope.mapMarkers = [];
        $scope.mapOptions = {
            center: new google.maps.LatLng(58.377926, 26.727380),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        function loadDataAndCenterOnFirstmarker() {
            if ($scope.requestErrors > 10) {
                clearInterval($scope.interval);
            }

            for (var i in $scope.mapMarkers) {
                if (!$scope.mapMarkers.hasOwnProperty(i)) {
                    continue;
                }

                $scope.mapMarkers[i].setMap(null);
            }

            $http.get('api.php', {params: {apiKey: $scope.apiKey, what: 'task1'}}).success(function (data) {
                $scope.table = data;

                var posFirst = null;
                for (var i = 0; i < data.length; i++) {
                    var pos = new google.maps.LatLng(data[i].lat, data[i].lng);

                    if (i == 0) {
                        posFirst = pos;
                    }

                    $scope.mapMarkers.push(new google.maps.Marker({
                        map: $scope.test1Map,
                        position: pos
                    }));
                }

                $scope.test1Map.setCenter(posFirst);

                $scope.indicator = false;
                $scope.requestErrors = 0;
            }).error(function (data) {
                $scope.requestErrors += 1;
                $scope.indicator = false;
                $scope.error = data.message;
            });
        }

        $scope.submit = function () {
            $scope.indicator = true;
            $scope.error = false;
            loadDataAndCenterOnFirstmarker();

            $scope.interval = setInterval(function () {
                loadDataAndCenterOnFirstmarker();
            }, 5000);
        };

        $scope.$on('$routeChangeStart', function (scope, next, current) {
            clearInterval(scope.interval);
        });
    }
);
seemeApp.controller('Test2Controller', function ($scope, $http, $location) {
        $scope.indicator = false;
        $scope.table = [];
        $scope.polygons = [];
        $scope.mapPolygons = [];
        $scope.mapMarkers = [];
        $scope.mapOptions = {
            center: new google.maps.LatLng(58.377926, 26.727380),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.colorMap = {};
        $scope.activeObjects = {};

        function loadDataAndCenterOnFirstmarker() {
            if ($scope.requestErrors > 10) {
                return;
            }

            $http.get('api.php', {params: {apiKey: $scope.apiKey, what: 'task2'}}).success(function (data) {
                $scope.table = data.table;
                $scope.polygons = data.polygons;

                $scope.indicator = false;
                $scope.requestErrors = 0;
            }).error(function (data) {
                $scope.requestErrors += 1;
                $scope.indicator = false;
                $scope.error = data.message;
            });
        }

        $scope.submit = function () {
            $scope.indicator = true;
            $scope.error = false;
            loadDataAndCenterOnFirstmarker();
        };

        $scope.showOnMap = function (objectName1, objectName2, value) {
            if (!$scope.polygons || value == 0) {
                return;
            }

            for (var m = 0; m < $scope.mapPolygons.length; m++) {
                $scope.mapPolygons[m].setMap(null);
            }
            for (var i in $scope.mapMarkers) {
                if (!$scope.mapMarkers.hasOwnProperty(i)) {
                    continue;
                }

                $scope.mapMarkers[i].setMap(null);
            }

            $scope.colorMap[objectName1] = {};
            $scope.colorMap[objectName1].stroke = '#800000';
            $scope.colorMap[objectName1].fill = '#FF0000';
            $scope.colorMap[objectName2] = {};
            $scope.colorMap[objectName2].stroke = '#006000';
            $scope.colorMap[objectName2].fill = '#00FF00';

            function drawPolygon(objectName) {
                if (!$scope.polygons[objectName]) {
                    return;
                }

                var coords = [];
                var coordFirst = null;
                var ci = 0;
                for (var c in $scope.polygons[objectName]) {
                    if (!$scope.polygons[objectName].hasOwnProperty(c)) {
                        continue;
                    }
                    coords.push(new google.maps.LatLng($scope.polygons[objectName][c].lat, $scope.polygons[objectName][c].lng));
                    if (ci === 0) {
                        coordFirst = new google.maps.LatLng($scope.polygons[objectName][c].lat, $scope.polygons[objectName][c].lng);
                        $scope.mapMarkers.push(new google.maps.Marker({
                            map: $scope.test2Map,
                            position: coordFirst
                        }));
                    }
                    ci++;
                }
                var polygon = new google.maps.Polygon({
                    paths: coords,
                    strokeColor: $scope.colorMap[objectName].stroke,
                    strokeOpacity: 0.8,
                    strokeWeight: 1,
                    fillColor: $scope.colorMap[objectName].fill,
                    fillOpacity: 0.35
                });
                polygon.setMap($scope.test2Map);
                $scope.mapPolygons.push(polygon);

                $scope.test2Map.setCenter(coordFirst);
            }

            drawPolygon(objectName1);
            drawPolygon(objectName2);

            $scope.activeObjects[0] = objectName1;
            $scope.activeObjects[1] = objectName2;

            $scope.mapOptions = {
                zoom: 8
            };
        };
    }
);