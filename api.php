<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/1/14 12:03 AM
 * @version 1.0
 */
require 'lib/iController.php';
require 'lib/SeeMeException.php';
require 'lib/XmlHelper.php';
require 'lib/TaskAbstract.php';
require 'lib/Task1Controller.php';
require 'lib/Task2Controller.php';

use TestLib\iController;
use TestLib\SeeMeException;
use TestLib\Task1Controller;
use TestLib\Task2Controller;

error_reporting(0);

$apiKeys = array(
    'proovitoo1',
    'proovitoo2',
);


$response = array();
$ctrl = null;

try {
    if (!isset($_GET['apiKey']) || !in_array($_GET['apiKey'], $apiKeys)) {
        throw new SeeMeException('Invalid API key', SeeMeException::CODE_PUBLIC);
    }

    if (!isset($_GET['what'])) {
        throw new SeeMeException('Invalid action', SeeMeException::CODE_PUBLIC);
    }

    switch ($_GET['what']) {
        case 'task1':
            $ctrl = new Task1Controller();
            break;
        case 'task2':
            $ctrl = new Task2Controller();
            break;
        default:
            throw new SeeMeException('Unknown action', SeeMeException::CODE_PUBLIC);
    }

    if ($ctrl instanceof iController) {
        $response = $ctrl->response();
    }
} catch (SeeMeException $e) {
    header('HTTP/1.1 400 Bad Request', true, 400);
    $response = array('message' => $e->getCode() == SeeMeException::CODE_PUBLIC ? $e->getMessage() : 'Error: See logs');
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'log.txt', $e->getMessage() . ":\n" . $e->getTraceAsString() . "\n\n\n", FILE_APPEND);
} catch (Exception $e) {
    header('HTTP/1.1 500 Server error', true, 500);
    $response = array('message' => 'Error!');
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'log.txt', $e->getMessage() . ":\n" . $e->getTraceAsString() . "\n\n\n", FILE_APPEND);
}


if (is_array($response) || is_object($response)) {
    header('Content-Type: application/json');
    echo json_encode($response, JSON_NUMERIC_CHECK);
} elseif (is_scalar($response)) {
    echo $response;
} else {
    echo '';
}